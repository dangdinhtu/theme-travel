<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2014 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Mon, 11 Aug 2014 12:40:23 GMT
 */

if( !defined( 'NV_MAINFILE' ) )
	die( 'Stop!!!' );

function nv4_block_package3( $block_config )
{
	$xtpl = new XTemplate( 'global.package3.tpl', NV_ROOTDIR . '/themes/travel/blocks/' );
	$xtpl->assign( 'CAPTION', 'Tính toán' );

	for( $i = 0; $i < 10; $i++ )
	{
		// Tính toán biến
		$arr = array( );
		$arr['key'] = $i;
		$arr['value'] = $i * $i;

		// TRuyền kết quả từ PHP sang tempalte
		$xtpl->assign( 'KQ', $arr );
		$xtpl->parse( 'main.vonglap' );
	}

	$xtpl->parse( 'main' );
	return $xtpl->text( 'main' );

}

if( defined( 'NV_SYSTEM' ) )
{
	$content = nv4_block_package3( $block_config );
}
?>