<!-- BEGIN: mainblock -->

<div class="panel panel-default">

	<div class="panel-body">
		<h4 class="adjst">{BLOCK_TITLE}</h4>
		<p>
			{BLOCK_CONTENT}
		</p>

	</div>
</div>
<!--  END: mainblock -->