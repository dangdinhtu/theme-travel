 
		
		<!--FOOTER SECTION -->
		<div id="footer">
			[FOOTER]
		</div>
		<!-- END FOOTER SECTION -->

		<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
		<!-- CORE JQUERY  -->
		<script src="plugins/jquery-1.10.2.js"></script>
		<!-- BOOTSTRAP CORE SCRIPT   -->
		<script src="plugins/bootstrap.min.js"></script>
		<!-- ISOTOPE SCRIPT   -->
		<script src="plugins/jquery.isotope.min.js"></script>
		<!-- PRETTY PHOTO SCRIPT   -->
		<script src="plugins/jquery.prettyPhoto.js"></script>
		<!-- CUSTOM SCRIPTS -->
		<script src="{NV_BASE_SITEURL}themes/{TEMPLATE}/js/custom.js"></script>

	</body>
</html>