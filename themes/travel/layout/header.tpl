﻿<!DOCTYPE html> <!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
	<!--<![endif]-->
	<head>
		{THEME_PAGE_TITLE}
		{THEME_META_TAGS}

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!--[if IE]>

		<![endif]-->

		<!--REQUIRED STYLE SHEETS-->
		<!-- BOOTSTRAP CORE STYLE CSS -->
		<link href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/bootstrap.css" rel="stylesheet" />
		<!-- FONTAWESOME STYLE CSS -->
		<link href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/font-awesome.min.css" rel="stylesheet" />
		<!--ANIMATED FONTAWESOME STYLE CSS -->
		<link href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/font-awesome-animation.css" rel="stylesheet" />
		<!--PRETTYPHOTO MAIN STYLE -->
		<link href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/prettyPhoto.css" rel="stylesheet" />
		<!-- CUSTOM STYLE CSS -->
		<link href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/style.css" rel="stylesheet" />
		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		{THEME_CSS}
		{THEME_SITE_RSS}
		{THEME_SITE_JS}
	</head>
	<body>
		<!-- NAV SECTION -->
		<div class="navbar navbar-inverse navbar-fixed-top">

			<div class="container">
				
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{NV_BASE_SITEURL}">YOUR LOGO</a>
				</div>
				<div class="navbar-collapse collapse">
					[MENU]
				</div>				
				
			</div>
		</div>
		<!--END NAV SECTION -->

		<!--HOME SECTION-->
