<!-- BEGIN: main -->
{FILE "header.tpl"}
		<section  id="services-sec">
			<div class="container">
			
			
				<div class="row ">
					<div class="text-center g-pad-bottom">
						{MODULE_CONTENT} 
					</div>
				</div>
				<div class="row go-marg">

					<div class="col-md-4 col-sm-4">
						[PACKAGE1]
					</div>
					<div class="col-md-4 col-sm-4">
						[PACKAGE2]
					</div>
					<div class="col-md-4 col-sm-4">
						[PACKAGE3]
					</div>
				</div>

			</div>
		</section>
		<!--END HOME SECTION-->

{FILE "footer.tpl"}
<!-- END: main -->