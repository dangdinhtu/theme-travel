<!-- BEGIN: main -->
{FILE "header.tpl"}
 
		<div id="home-sec">

			<div class="container"  >
				<div class="row text-center">
					<div  class="col-md-12" >
						<span class="head-main" >Travel Template</span>
						<h3 class="head-last col-md-4 col-md-offset-4  col-sm-6 col-sm-offset-3">Lorem ipsum dolor sit ametLorem</h3>

					</div>
				</div>
			</div>
		</div>

		<section  id="services-sec">
			<div class="container">
			
			
				<div class="row ">
					<div class="text-center g-pad-bottom">
						{MODULE_CONTENT} 
					</div>
				</div>
				<div class="row go-marg">

					<div class="col-md-4 col-sm-4">
						[PACKAGE1]
					</div>
					<div class="col-md-4 col-sm-4">
						[PACKAGE2]
					</div>
					<div class="col-md-4 col-sm-4">
						[PACKAGE3]
					</div>
				</div>

			</div>
		</section>
		<!--END HOME SECTION-->

{FILE "footer.tpl"}
<!-- END: main -->